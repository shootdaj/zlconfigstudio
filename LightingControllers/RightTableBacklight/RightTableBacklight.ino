#include <WiFiUdp.h>
#include <WiFiServer.h>
#include <WiFiClientSecure.h>
#include <WebSocketsClient.h>
#include <WebSockets.h>
#include <ws2812_i2s.h>
#include <WebSocketsServer.h>
#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WiFiMulti.h>
#include <ESP8266HTTPClient.h>
#include <FS.h>
#include <DNSServer.h>
#include <ESP8266WebServer.h>
#include <WiFiManager.h>
#include <Ticker.h>
#include <ESP8266httpUpdate.h>

#define NUM_LEDS 60
#define STATUS_LED 2

int delayTime = 15;

const char* ssid = "Pterodactyl";
const char* password = "n00bn00b";
const char * AuthUsername = "shootdaj"; //change
const char * AuthPassword = "n00bn00b"; //change
String DeviceName = "RightTableBacklight"; //change
String WSServerIP = "192.168.29.100"; //change
int WSServerPort = 22229; //change
int ServerPort = 9999; //change
String LogEntriesToken = "6bffda3c-b803-42cf-b98e-8be6f9dc71bd"; //change
String BinaryVersion = "testversion";

String ServerBaseURL = "http://" + WSServerIP + ":" + ServerPort;
String LogURL = "http://webhook.logentries.com/noformat/logs/" + LogEntriesToken;
String DelayTimeURL = ServerBaseURL + "/Configuration/GetDelayTime";
static WS2812 LedStrip;
static Pixel_t Pixels[NUM_LEDS];
HTTPClient HTTP;
WebSocketsClient WebSocketClient;
ESP8266WiFiMulti WiFiMulti;

char mqtt_server[40];

void setup() {
	Serial.begin(115200);
	pinMode(STATUS_LED, OUTPUT);
	LEDOFF();

	//RunUpdateLogic();

	LedStrip.init(NUM_LEDS);

	//WiFiManager wifiManager;
	////WiFiManagerParameter custom_mqtt_server("server", "mqtt server", mqtt_server, 40);
	////wifiManager.addParameter(&custom_mqtt_server);
	//wifiManager.resetSettings();
	////wifiManager.autoConnect();

	//wifiManager.startConfigPortal("ZL_TestNew");


	//connect to wifi
	WiFiMulti.addAP(ssid, password);
	bool status = HIGH;
	while (WiFiMulti.run() != WL_CONNECTED) {
		delay(500);
		digitalWrite(2, status);
		status = !status;
	}

	digitalWrite(2, LOW);

	Serial.printf("Connected to WIFI\nIP address: ");
	Serial.println(WiFi.localIP());
	
	byte mac[6];

	WiFi.macAddress(mac);
	Serial.print("MAC: ");
	Serial.print(mac[5], HEX);
	Serial.print(":");
	Serial.print(mac[4], HEX);
	Serial.print(":");
	Serial.print(mac[3], HEX);
	Serial.print(":");
	Serial.print(mac[2], HEX);
	Serial.print(":");
	Serial.print(mac[1], HEX);
	Serial.print(":");
	Serial.println(mac[0], HEX);

	LogIP();
	//auto delayTimeResponse = GetDelayTime();
	//delayTime = delayTimeResponse.toInt();
	//LogDeviceNameAndDelayTime(delayTimeResponse);

	WebSocketClient.setAuthorization(AuthUsername, AuthPassword);
	WebSocketClient.begin(WSServerIP, WSServerPort, "/", "");
	WebSocketClient.onEvent(WebSocketEvent);

	SendHTTPRequest(LogURL, DeviceName + ": WebSocketClient started");

	LEDOn();	
}

void loop() {
	WebSocketClient.loop();
	delay(delayTime);
}

void RunUpdateLogic()
{
	auto returnValue = ESPhttpUpdate.update(WSServerIP, ServerPort, "/Update/GetUpdate", BinaryVersion);
}


void LEDOn()
{
	digitalWrite(STATUS_LED, LOW);
}

void LEDOFF()
{
	digitalWrite(STATUS_LED, HIGH);
}

String SendHTTPRequest(String url, String body)
{
	Serial.print(body + "\n");

	HTTP.begin(url);
	HTTP.addHeader("Content-Type", "application/json");
	HTTP.POST(body);

	auto response = HTTP.getString();

	HTTP.end();

	return response;
}

void LogIP()
{
	auto myIP = WiFi.localIP();
	String ipStr = String(myIP[0]) + "." + String(myIP[1]) + "." + String(myIP[2]) + "." + String(myIP[3]);
	String body = DeviceName + ": " + ipStr;
	SendHTTPRequest(LogURL, body);
}

String GetDelayTime()
{
	SendHTTPRequest(LogURL, DeviceName + ": Getting delay time");
	auto body = "\"" + DeviceName + "\"";
	auto delayTimeResponse = SendHTTPRequest(DelayTimeURL, body);
	return delayTimeResponse;
}

void LogDeviceNameAndDelayTime(String delayTimeResponse)
{
	SendHTTPRequest(LogURL, DeviceName + ": " + delayTimeResponse);
}

void WebSocketEvent(WStype_t type, uint8_t * payload, size_t length) {
	if (length > 0)
	{
		for (int i = 0; i < NUM_LEDS; i++)
		{
			Pixels[i].R = payload[i * 3];
			Pixels[i].G = payload[i * 3 + 1];
			Pixels[i].B = payload[i * 3 + 2];
		}

		LedStrip.show(Pixels);
		delay(delayTime);
	}
}

